This is an example of a model applied on relatively trivial dataset, that could be adjusted to your needs. 

Dataset we're looking at contains date and number of transactions of a certain retail shop.

The data varies a fair bit, takes into account COVID-19 pandemic hit and drop in sales. That's what makes it difficult to predict into the future
at this point in time. 



